<?php

namespace App\Subscriber;

use App\Entity\Intervention;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class InterventionPersistSubscriber implements EventSubscriberInterface
{

    public function __construct(
        private TokenStorageInterface $tokenStorage
    ) {
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Intervention) {
            return;
        }

        // Pour DataFixture
        if (!$this->tokenStorage->getToken() || get_class($this->tokenStorage->getToken()->getUser()) === AppFixtures::class) {
            return;
        }

        $token = $this->tokenStorage->getToken();
        $user = $token->getUser();

        $entity->setDateInterventationAt(new \DateTimeImmutable);
        $entity->setEmploye($user);
    }
}
