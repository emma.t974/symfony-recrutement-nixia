<?php

namespace App\Form;

use App\Entity\Client;
use App\Entity\Intervention;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InterventionType extends AbstractType
{

    public function __construct(
        private RequestStack $request
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $request = $this->request->getCurrentRequest();
        if ($request->query->get('client')) {
            $userId = $request->query->get('client');
            $builder
                ->add('client', EntityType::class, [
                    "class" => Client::class,
                    "multiple" => false,
                    "choice_label" => function (Client $client) {
                        return $client->getNomComplet();
                    },
                    "query_builder" => function (EntityRepository $er) use ($userId) {
                        return $er->createQueryBuilder('c')
                            ->where('c.id = :clientId')
                            ->setParameter('clientId', $userId);
                    },
                    'by_reference' => false,
                    'attr' => [
                        'disabled' => true
                    ]
                ]);
        } else {
            $builder
                ->add('client', EntityType::class, [
                    "class" => Client::class,
                    "multiple" => false,
                    "choice_label" => function (Client $client) {
                        return $client->getNomComplet();
                    },
                    "query_builder" => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c');
                    },
                    'by_reference' => false,
                    'attr' => [
                        'class' => 'select-employees'
                    ]
                ]);
        }
        $builder

            ->add('intitule', TextType::class, [
                'label' => 'Intitulé'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description'
            ])
            ->add('tempsIntervention', TimeType::class, [
                'label' => 'Temps d\'intervention'
            ])
            ->add('signature', HiddenType::class)
            ->add('Valider', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Intervention::class,
        ]);
    }
}
