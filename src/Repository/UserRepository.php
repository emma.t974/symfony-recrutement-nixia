<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class UserRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry
    ) {
        parent::__construct($registry, User::class);
    }

    public function findByKeyword(string $keyword)
    {
        return $this->createQueryBuilder('u')
            ->where('u.prenom LIKE :keyword')
            ->orWhere('u.nom LIKE :keyword')
            ->setParameter('keyword', "%$keyword%")
            ->getQuery()
            ->getResult();
    }
}
