<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Table("users")]
#[ORM\Entity(repositoryClass: UserRepository::class)]
class User implements UserInterface
{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'string')]
    #[Length(
        min: 3,
        max: 50,
        minMessage: "Le prénom saisir est trop petit",
        maxMessage: "Le prénom saisir est trop grand"
    )]
    #[NotNull(
        message: "Le champ ne peux pas être vide"
    )]
    #[NotBlank(
        message: "Le champ ne peux être vide"
    )]
    private ?string $prenom = null;

    #[ORM\Column(type: 'string')]
    #[Length(
        min: 3,
        max: 50,
        minMessage: "Le nom saisir est trop petit",
        maxMessage: "Le nom saisir est trop grand"
    )]
    #[NotNull(
        message: "Le champ ne peut pas être vide"
    )]
    #[NotBlank(
        message: "Le champ ne peut pas être vide"
    )]
    private ?string $nom = null;

    #[ORM\Column(type: 'integer', unique: true)]
    #[Length(
        min: 4,
        max: 4,
        minMessage: "Le code pin est trop petit",
        maxMessage: "Le code pin est trop grand"
    )]
    #[NotNull(
        message: "Le champ ne peut pas être vide"
    )]
    #[NotBlank(
        message: "Le champ ne peut pas être vide"
    )]
    private ?int $pin = null;

    #[ORM\Column(type: 'json')]
    private array $roles = [];

    #[ORM\OneToMany(mappedBy: 'employe', targetEntity: Intervention::class)]
    private Collection $interventions;

    public function __construct()
    {
        $this->interventions = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPin(): ?int
    {
        return $this->pin;
    }

    public function setPin(int $pin): self
    {
        $this->pin = $pin;
        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;

        $roles[] = 'ROLE_EMPLOYEE';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getUsername()
    {
        return (string) $this->pin;
    }

    public function eraseCredentials()
    {
    }

    // On se connecte via code pin donc pas besoin
    public function getPassword()
    {
        return null;
    }

    // On se connecte via code pin donc pas besoin
    public function getSalt()
    {
        return null;
    }

    public function getUserIdentifier(): string
    {
        return (string) $this->pin;
    }

    /**
     * @return Collection<int, Intervention>
     */
    public function getInterventions(): Collection
    {
        return $this->interventions;
    }

    public function addIntervention(Intervention $intervention): self
    {
        if (!$this->interventions->contains($intervention)) {
            $this->interventions->add($intervention);
            $intervention->setEmploye($this);
        }

        return $this;
    }

    public function removeIntervention(Intervention $intervention): self
    {
        if ($this->interventions->removeElement($intervention)) {
            // set the owning side to null (unless already changed)
            if ($intervention->getEmploye() === $this) {
                $intervention->setEmploye(null);
            }
        }

        return $this;
    }

    public function getNomComplet(): ?string
    {
        return $this->nom . ' ' . $this->prenom;
    }
}
