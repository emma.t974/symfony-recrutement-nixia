<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: ClientRepository::class)]
#[ORM\Table(name: 'clients')]
class Client
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    #[NotNull(
        message: "Le champ ne peut pas être vide"
    )]
    #[NotBlank(
        message: "Le champ ne peut pas être vide"
    )]
    #[Length(
        min: 3,
        max: 50,
        minMessage: 'Le nom saisi est trop petit',
        maxMessage: 'Le nom saisi est trop grand'
    )]
    private ?string $nom = null;

    #[ORM\Column]
    #[NotNull(
        message: "Le champ ne peut pas être vide"
    )]
    #[NotBlank(
        message: "Le champ ne peut pas être vide"
    )]
    #[Length(
        min: 3,
        max: 50,
        minMessage: 'Le prénom saisi est trop petit',
        maxMessage: 'Le prénom saisi est trop grand'
    )]
    private ?string $prenom = null;

    #[ORM\Column]
    #[NotNull(
        message: "Le champ ne peut pas être vide"
    )]
    #[NotBlank(
        message: "Le champ ne peut pas être vide"
    )]
    #[Email(message: "L'email saisi n'est pas valide")]
    private ?string $email = null;

    #[ORM\Column]
    #[NotNull(
        message: "Le champ ne peut pas être vide"
    )]
    #[NotBlank(
        message: "Le champ ne peut pas être vide"
    )]
    private ?int $fixe = null;

    #[ORM\Column]
    #[NotNull(
        message: "Le champ ne peut pas être vide"
    )]
    #[NotBlank(
        message: "Le champ ne peut pas être vide"
    )]
    private ?int $portable = null;

    #[ORM\Column(type: Types::TEXT)]
    #[NotNull(
        message: "Le champ ne peut pas être vide"
    )]
    #[NotBlank(
        message: "Le champ ne peut pas être vide"
    )]
    private ?string $adresse = null;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: Intervention::class, orphanRemoval: true)]
    private Collection $interventions;

    public function __construct()
    {
        $this->interventions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFixe(): ?int
    {
        return $this->fixe;
    }

    public function setFixe(int $fixe): self
    {
        $this->fixe = $fixe;

        return $this;
    }

    public function getPortable(): ?int
    {
        return $this->portable;
    }

    public function setPortable(int $portable): self
    {
        $this->portable = $portable;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return Collection<int, Intervention>
     */
    public function getInterventions(): Collection
    {
        return $this->interventions;
    }

    public function addIntervention(Intervention $intervention): self
    {
        if (!$this->interventions->contains($intervention)) {
            $this->interventions->add($intervention);
            $intervention->setClient($this);
        }

        return $this;
    }

    public function removeIntervention(Intervention $intervention): self
    {
        if ($this->interventions->removeElement($intervention)) {
            // set the owning side to null (unless already changed)
            if ($intervention->getClient() === $this) {
                $intervention->setClient(null);
            }
        }

        return $this;
    }

    public function getNomComplet(): ?string
    {
        return $this->nom . ' ' . $this->prenom;
    }
}
