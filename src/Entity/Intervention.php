<?php

namespace App\Entity;

use App\Repository\InterventionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: InterventionRepository::class)]
class Intervention
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    #[NotNull(
        message: "Le champ ne peut pas être vide"
    )]
    #[NotBlank(
        message: "Le champ ne peut pas être vide"
    )]
    #[Length(
        min: 3,
        max: 200,
        minMessage: 'L\'intitulé saisi est trop petit',
        maxMessage: 'L\'intitulé saisi est trop grand'
    )]
    private ?string $intitule = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $dateInterventationAt = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $tempsIntervention = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $signature = null;

    #[ORM\ManyToOne(inversedBy: 'interventions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Client $client = null;

    #[ORM\ManyToOne(inversedBy: 'interventions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $employe = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateInterventationAt(): ?\DateTimeImmutable
    {
        return $this->dateInterventationAt;
    }

    public function setDateInterventationAt(\DateTimeImmutable $dateInterventationAt): self
    {
        $this->dateInterventationAt = $dateInterventationAt;

        return $this;
    }

    public function getTempsIntervention(): ?\DateTimeInterface
    {
        return $this->tempsIntervention;
    }

    public function setTempsIntervention(\DateTimeInterface $tempsIntervention): self
    {
        $this->tempsIntervention = $tempsIntervention;

        return $this;
    }

    public function getSignature(): ?string
    {
        return $this->signature;
    }

    public function setSignature(string $signature): self
    {
        $this->signature = $signature;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getEmploye(): ?User
    {
        return $this->employe;
    }

    public function setEmploye(?User $employe): self
    {
        $this->employe = $employe;

        return $this;
    }
}
