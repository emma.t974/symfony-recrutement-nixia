<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EmployeType;

final class EmployeController extends CrudController
{
    protected $formType = EmployeType::class;
    protected $entity = User::class;
    protected $templatePath = 'employe';
}
