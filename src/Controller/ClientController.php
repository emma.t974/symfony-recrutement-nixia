<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;

final class ClientController extends CrudController
{
    protected $formType = ClientType::class;
    protected $entity = Client::class;
    protected $templatePath = 'client';
}
