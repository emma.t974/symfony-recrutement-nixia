<?php

namespace App\Controller;

use App\Entity\Intervention;
use App\Form\InterventionType;

final class InterventionController extends CrudController
{
    protected $formType = InterventionType::class;
    protected $entity = Intervention::class;
    protected $templatePath = 'intervention';
}
