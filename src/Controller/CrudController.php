<?php

namespace App\Controller;

use App\Form\FormSearchType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class CrudController extends AbstractController
{
    protected $formType;
    protected $entity;
    protected $templatePath;

    public function __construct(
        private EntityManagerInterface $em
    ) {
    }

    public function list(Request $request): Response
    {

        $form = $this->createForm(FormSearchType::class)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $data = $this->em->getRepository($this->entity)->findByKeyword($formData['keyword']);
        } else {
            $data = $this->em->getRepository($this->entity)->findAll();
        }

        return $this->render($this->templatePath . '/list.html.twig', [
            $this->templatePath . 's' => $data,
            "form" => $form->createView()
        ]);
    }

    public function add(
        Request $request
    ): Response {
        $data = new $this->entity;
        $form = $this->createForm($this->formType, $data)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($data);
            $this->em->flush();
            $this->addFlash('success', 'L\'élément a bien été ajouté.');
            return $this->redirectToRoute('app_' . $this->templatePath . '_show', ['id' => $data->getId()]);
        }

        return $this->render($this->templatePath . '/_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function edit(
        int $id,
        Request $request
    ): Response {
        $data = $this->em->getRepository($this->entity)->find($id);
        $form = $this->createForm($this->formType, $data)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', ' L\'élément a bien été modifié.');
            return $this->redirectToRoute('app_' . $this->templatePath . '_show', ['id' => $data->getId()]);
        }

        return $this->render($this->templatePath . '/_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function show(
        int $id,
    ): Response {
        $data = $this->em->getRepository($this->entity)->find($id);
        return $this->render($this->templatePath . '/show.html.twig', [
            $this->templatePath => $data
        ]);
    }
}
