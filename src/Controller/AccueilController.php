<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class AccueilController extends AbstractController
{

    #[Route("/", name: "app_home", methods: ['GET'])]
    public function home(): Response
    {
        return $this->render('home.html.twig');
    }
}
