import './styles/app.scss'
import './bootstrap'
import SignatureInput from './custom-element/SignatureElement'

import * as bootstrap from 'bootstrap';

window.onload = () => {
    let canvas = new SignatureInput("#signature")

    document.querySelector('#removeSignature').addEventListener('click', (e) => {
        e.preventDefault()
        canvas.remove()
    })
}