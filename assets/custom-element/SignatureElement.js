export default class SignatureInput {
    constructor(canvas) {
        this.sign = false
        this.prevX = 0
        this.prevY = 0

        this.canvas = document.querySelector(canvas)
        this.ctx = this.canvas.getContext("2d")
        this.ctx.strokeStyle = "black"
        this.ctx.lineWidth = 2

        this.canvas.addEventListener("mousedown", (e) => {
            this.mouseDown(e)
        })

        this.canvas.addEventListener("touchstart", (e) => {
            this.mouseDown(e)
        })

        this.canvas.addEventListener("mousemove", (e) => {
            this.mouseMove(e)
        })

        this.canvas.addEventListener("touchmove", (e) => {
            this.mouseMove(e)
        })

        this.canvas.addEventListener("mouseup", () => {
            this.mouseUp()
        })

        this.canvas.addEventListener("touchend", () => {
            this.mouseUp()
        })


        this.canvas.addEventListener("mouseout", () => {
            this.mouseOut()
        })
    }

    mouseDown(e) {
        this.sign = true
        this.prevX = e.clientX - this.canvas.offsetLeft
        this.prevY = e.clientY - this.canvas.offsetTop
        this.disableBodyScroll()
    }

    mouseMove(e) {
        if (this.sign) {
            let currX = e.clientX - this.canvas.offsetLeft
            let currY = e.clientY - this.canvas.offsetTop

            this.draw(this.prevX, this.prevY, currX, currY)
            this.prevX = currX
            this.prevY = currY
        }
    }

    mouseUp() {
        this.sign = false
        this.generateImageBase64()
        this.enableBodyScroll()
    }

    mouseOut() {
        this.sign = false
        this.enableBodyScroll()
    }

    draw(startX, startY, endX, endY) {
        this.ctx.beginPath()
        this.ctx.moveTo(startX, startY)
        this.ctx.lineTo(endX, endY)
        this.ctx.closePath()
        this.ctx.stroke()
    }

    remove() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        document.querySelector('#intervention_signature').value = ''
    }

    generateImageBase64() {
        let image = this.canvas.toDataURL("image/png")
        document.querySelector('#intervention_signature').value = image
    }

    disableBodyScroll() {
        document.body.style.overflow = "hidden"
    }

    enableBodyScroll() {
        document.body.style.overflow = ""
    }
}