<?php

namespace App\Test;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class EmployeTest extends TestCase
{
    public function testNomComplet()
    {
        $user = new User();
        $user
            ->setNom('Doe')
            ->setPrenom('John');
        $nomComplet = $user->getNomComplet();
        $this->assertEquals('Doe John', $nomComplet);
    }

    public function testEntity()
    {
        $user = new User();
        $user->setNom('Doe')
            ->setPrenom('John')
            ->setRoles(['ROLE_EMPLOYEE'])
            ->setPin(1234);
        $this->assertEquals('Doe', $user->getNom());
        $this->assertEquals('John', $user->getPrenom());
        $this->assertEquals(['ROLE_EMPLOYEE'], $user->getRoles());
        $this->assertEquals(1234, $user->getPin());
    }
}
