<?php

namespace App\Test;

use App\Entity\Intervention;
use PHPUnit\Framework\TestCase;

class InterventionTest extends TestCase
{

    public function testEntity()
    {
        $intervention = new Intervention();
        $intervention->setIntitule('Intitulé test')
            ->setDescription('Description')
            ->setDateInterventationAt(new \DateTimeImmutable('00:00:00'))
            ->setTempsIntervention(new \DateTimeImmutable('00:15:00'))
            ->setSignature('base64:...');

        $this->assertEquals('Intitulé test', $intervention->getIntitule());
        $this->assertEquals('Description', $intervention->getDescription());
        $this->assertEquals('00:00:00', $intervention->getDateInterventationAt()->format('H:i:s'));
        $this->assertEquals('00:15:00', $intervention->getTempsIntervention()->format('H:i:s'));
        $this->assertEquals('base64:...', $intervention->getSignature());
    }
}
