<?php

namespace App\Test;

use App\Entity\Client;
use App\Entity\Intervention;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validation;

class ValidatorTest extends KernelTestCase
{
    public function testValidatorClient()
    {
        $client = new Client();

        $client->setNom('Doe')
            ->setPrenom('John')
            ->setPortable(123456789)
            ->setFixe(123456789)
            ->setAdresse('1 rue de la paix')
            ->setEmail('john@doe.com');

        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        $errors = $validator->validate($client);

        $this->assertCount(0, $errors);
    }

    public function testValidatorUser()
    {
        $user = new User();

        $user->setNom('Doe')
            ->setPrenom('John')
            ->setRoles(['ROLE_EMPLOYEE'])
            ->setPin(1234);


        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        $errors = $validator->validate($user);

        $this->assertCount(0, $errors);
    }

    public function testValidatorIntervention()
    {
        $intervention = new Intervention();
        $intervention->setIntitule('Intitulé test')
            ->setDescription('Description')
            ->setDateInterventationAt(new \DateTimeImmutable('00:00:00'))
            ->setTempsIntervention(new \DateTimeImmutable('00:15:00'))
            ->setSignature('base64:...');

        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

        $errors = $validator->validate($intervention);

        $this->assertCount(0, $errors);
    }
}
