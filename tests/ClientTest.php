<?php

namespace App\Test;

use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testNomComplet()
    {
        $client = new Client();
        $client->setNom('Doe');
        $client->setPrenom('John');
        $nomComplet = $client->getNomComplet();
        $this->assertEquals('Doe John', $nomComplet);
    }

    public function testEntity()
    {
        $client = new Client();
        $client->setNom('Doe')
            ->setPrenom('John')
            ->setPortable(123456789)
            ->setFixe(123456789)
            ->setAdresse('1 rue de la paix');
        $this->assertEquals('Doe', $client->getNom());
        $this->assertEquals('John', $client->getPrenom());
        $this->assertEquals(123456789, $client->getPortable());
        $this->assertEquals(123456789, $client->getFixe());
        $this->assertEquals('1 rue de la paix', $client->getAdresse());
    }
}
